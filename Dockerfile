FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /json_app
WORKDIR /json_app
COPY requirements.txt /json_app/
RUN pip3 install -r requirements.txt
COPY . /json_app/