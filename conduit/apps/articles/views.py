from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


from .models import Article
from .renderers import ArticleJSONRenderer
from .serializers import ArticleSerializer, ArticleUpdateSerializer
from rest_framework import viewsets


class ArticleRetrieveAPIView(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = ArticleSerializer

    def get_queryset(self):
        if self.request.user.is_staff or not self.request.user.is_authenticated:
            return Article.objects.all()
        else:
            author = self.request.user
            return Article.objects.filter(author_id=author.id)



class ArticleAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ArticleJSONRenderer,)
    serializer_class = ArticleSerializer

    def post(self, request):
        article = request.data.get('article', {})

        serializer = self.serializer_class(data=article)
        serializer.is_valid(raise_exception=True)
        serializer.save(request=request)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ArticleUpdate(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (IsAuthenticated,)
    def put(self, request, pk):
        snippet = Article.objects.get(id=pk)
        if request.user.is_staff or snippet.author == request.user:
            serializer = ArticleUpdateSerializer(snippet, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('You are neither an admin, nor owner of an article. Sorry!')


