from django.urls import path

from .views import ArticleRetrieveAPIView, ArticleAPIView, ArticleUpdate
from .counter import count_it

app_name = 'articles'
urlpatterns = [
    path('articles/', ArticleRetrieveAPIView.as_view({'get': 'list'})),
    path('articles/update/<int:pk>', ArticleUpdate.as_view()),
    path('articles/create/', ArticleAPIView.as_view()),
    path('articles/count/<int:article_id>', count_it),
]