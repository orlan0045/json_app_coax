from rest_framework import serializers

from .models import Article



class ArticleSerializer(serializers.ModelSerializer):
    title = serializers.CharField(allow_blank=False, required=True)
    body = serializers.CharField(allow_blank=True, required=False)
    image = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = ('id','body', 'title' ,'image',)

    def create(self, validated_data):
        title = validated_data['title']
        body = validated_data['body']
        request = validated_data['request']
        author = request.user
        return Article.objects.create(title=title,body=body,author=author)

    def get_image(self, obj):
        if obj.image:
            return obj.image

        return 'https://static.productionready.io/images/smiley-cyrus.jpg'

class ArticleUpdateSerializer(serializers.ModelSerializer):
    title = serializers.CharField(allow_blank=True, required=False)
    body = serializers.CharField(allow_blank=True, required=False)
    image = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = ('id', 'body', 'title', 'image',)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.body = validated_data.get('body', instance.body)
        instance.save()

        return instance


    def get_image(self, obj):
        if obj.image:
            return obj.image

        return 'https://static.productionready.io/images/smiley-cyrus.jpg'

