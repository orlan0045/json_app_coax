from collections import Counter
from .models import Article
from django.http.response import JsonResponse

def count_it(request, article_id):
    if article_id == 0:
        counted_total = Counter(a = 0)
        for article in Article.objects.all():
            counted_one = Counter(article.body.split())
            counted_total += counted_one
        return JsonResponse(counted_total)
    else:
        article = Article.objects.get(id=article_id)
        new_c = Counter(article.body.split())
        return JsonResponse(new_c)