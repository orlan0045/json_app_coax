from django.db import models
from conduit.apps.core.models import TimestampedModel


class Article(TimestampedModel):
    author = models.ForeignKey(
        'authentication.User', on_delete=models.CASCADE
    )
    body = models.TextField(blank=True)
    title = models.CharField(max_length=128)
    # In addition to the `bio` field, each user may have a profile image or
    # avatar. This field is not required and it may be blank.
    image = models.URLField(blank=True)

    def __str__(self):
        return self.title


